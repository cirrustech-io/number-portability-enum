#!/usr/bin/python
'''
This script is to be run daily using CRON.
It will download the latest NP Dump from NPCS
and generated a corresponding BIND configuration file 


Created on Jul 10, 2011

@author: Ali Khalil
'''

import syslog
from ported.dump.PortedDump import NpcdbDump

try:
    port_dump = NpcdbDump()
    
    filename = port_dump.getFile()
    
    if filename:
        #print filename
        syslog.syslog('File name: ' + filename)
        pass
    else:
        #print "File exists"
        syslog.syslog('File exists: yes')
        pass
    
    file_fullpath = port_dump.local_dir + port_dump.dumpFileToday()
    syslog.syslog('File Path: ' + file_fullpath)
    
    rawData = port_dump.readZipContent( file_fullpath )
    lines = port_dump.processCSV(rawData)
    #print lines
    conf = port_dump.genBIND(lines)
    #print conf
    
    
    template = port_dump.bindTemplate()
    
    import re
    import datetime
    
    pattern = re.compile('___SERNO___')
    
    today = datetime.date.today()
    serial_no = re.sub('-','',"%s"%(today)) + '01'
    
    # Add Serial Number to template
    conf_serno = re.sub('___SERNO___', serial_no, template)
    # Add Records count to above generated configuration
    conf_count = re.sub('___COUNT___', str(port_dump.records_total), conf_serno)
    # Add definition records to above generated configuration
    conf_full = re.sub('___RECORDS___', conf, conf_count)
    
    filename = '%s%s%s'%('port_dump_',today,'.zip')
    
    print conf_full
    #syslog.syslog(conf)
    
    
    import smtplib

    #syslog.syslog('IMPORT: Imported smtplib')
    fromaddr = 'npsync.alerts@2connectbahrain.com'
    toaddrs  = 'ali.khalil@2connectbahrain.com'
    subject = 'Numbers Ported: ' + str(port_dump.records_total)
    msg_body =  conf_full

    msg = '''\
FROM: %s
TO: %s
SUBJECT: %s

%s
''' % (fromaddr, toaddrs, subject, msg_body)

    # Credentials (if needed)
    #username = 'username'
    #password = 'password'


    smtp_host = 'mail.2connectbahrain.com'
    #smtp_port = 25
    # The actual mail send
    #server = smtplib.SMTP(smtp_host, smtp_port)
    server = smtplib.SMTP(smtp_host )
    #server.starttls()
    #server.login(username,password)
    server.sendmail(fromaddr, toaddrs, msg)
    server.quit()
    #syslog.syslog('message sent' )


    
        
except:
    syslog.syslog('''FAIL''')
    
