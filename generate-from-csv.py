#!/usr/bin/python
'''
This script is to be run daily using CRON.
It will download the latest NP Dump from NPCS
and generated a corresponding BIND configuration file 


Created on Jul 10, 2011

@author: Ali Khalil
'''

import syslog
from ported.dump.PortedDump import NpcdbDump

try:
    port_dump = NpcdbDump()
    filename = port_dump.getFile()
    
    if filename == False:
        syslog.syslog('Problem with getting file.')
        raise
    
    file_fullpath = port_dump.local_dir + port_dump.dumpFileToday()
    
    rawData = port_dump.readZipContent( file_fullpath )
    records = port_dump.processCSV(rawData)
    naptr_configuration = port_dump.genNAPTR(records)
    zone_configuration = port_dump.genZoneConf(naptr_configuration)
    print zone_configuration
    
    
    # Send Email 
    import smtplib
    fromaddr = 'npsync.alerts@2connectbahrain.com'
    toaddrs  = 'ali.khalil@2connectbahrain.com, number-portability@2connectbahrain.com'
    subject = 'Numbers Ported: ' + str(port_dump.count)
    msg_body =  zone_configuration

    msg = '''\
FROM: %s
TO: %s
SUBJECT: %s

%s
''' % (fromaddr, toaddrs, subject, msg_body)

    smtp_host = 'mail.2connectbahrain.com'
    server = smtplib.SMTP(smtp_host )
    server.sendmail(fromaddr, toaddrs, msg)
    server.quit()
    self.log('SMTP: Email notification sent.' )
    
    self.log('''Script Ended Successfully.''')
except:
    syslog.syslog('''Script Ended with Exception.''')
