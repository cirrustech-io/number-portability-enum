#!/usr/bin/python
'''
This script will empty the current ported_numbers table and
add all provided CSV records in to table

Created on Jul 23, 2011

@author: Ali Khalil
'''

import syslog
from ported.dump.PortedDump import NpcdbDump

try:
    port_dump = NpcdbDump()
    #filename = port_dump.getFile()
    filename = '/var/np/npcs-dump/port_dump_2012-10-16.zip'
    
    if filename:
        pass
    else:
        syslog.syslog('Problem with getting file.')
        raise
    
    file_fullpath = port_dump.local_dir + port_dump.dumpFileToday()
    rawData = port_dump.readZipContent( file_fullpath )
    records = port_dump.processCSV(rawData)
    syslog.syslog('''records retrieved''')
    print '''records retrieved'''
    port_dump.loadSQL(records)
    print '''Inserted %s records'''%(len(records))
    syslog.syslog('''Script ended successfully.''')
        
except:
    syslog.syslog('''Script ended with exception.''')
    import sys
    print sys.exc_info()
