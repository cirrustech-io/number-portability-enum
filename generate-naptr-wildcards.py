#!/usr/bin/python
'''
Generate the wildcard records

Created on Jul 14, 2011

@author: Ali Khalil
'''


import syslog
from ported.dump.PortedDump import NpcdbDump

try:
    port_dump = NpcdbDump()
    wildcards_configuration = port_dump.genWildcards()
    print wildcards_configuration
    #filename = "/tmp/ported-wildcards.log"
    #file = open(filename, 'w')
    #file.write(wildcards_configuration)
    #file.close()
    
    syslog.syslog('''Script Ended Successfully.''')
except:
    syslog.syslog('''Script Ended with Exception.''')
