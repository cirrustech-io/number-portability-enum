'''
Generate the wildcard records

Created on Jul 14, 2011

@author: Ali Khalil
'''



def process_range(base, digits, length ): #, file):
    output = ''
    if length > 1:
        # Loop through each digit for this range
        for digit in digits:
            new_base = str(digit) + "." + base
            
            output += "%s%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (
                            "*." + new_base,
                            '',
                            'NAPTR',
                            '100',
                            '10',
                            '"U"',
                            '"sip+E2U"',
                            '"!^(.*)$!reject31.com!"',
                            '.'
                            )

            content = process_range( new_base, digits, length-1 )
            if content != False:
                output += content  #+ "\n"#, file)
        return output
    else:
        return False    


def genWildcards():
    import re
    
    # Store the complete wildcards configuration here
    conf_wildcards = ''
    
    # Length of additional numbers after the range digits (total = 8)
    length = 7
    
    # Open File
    filename = "ported-wildcards.log"
    file = open(filename, 'w')
    
    # The ranges for phone numbers e.g. 33xxxxxx, 36xxxxxx, 39xxxxxx
    ranges = [ 
        (0, 5),
        (1, 8),
        (3, 8),
        (6, 8),
        (7, 8),
        (8, 8),
        (9 ,8),
        ]
    
    # Digits in the numbers (duh!)
    digits = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
    
    # Set pattern for modifying the ranges to reverse format with . separating each digit
    pattern = re.compile('(.)')
    
    # Loop through each range
    for range in ranges:
        # Run recursive configuration generation
        conf_wildcards += process_range(str(range[0]), digits, range[1]-1 ) # , file)
    
    conf_unknown = "%s%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (
                            "*",
                            '',
                            'NAPTR',
                            '100',
                            '10',
                            '"U"',
                            '"sip+E2U"',
                            '"!^(.*)$!reject31.com!"',
                            '.'
                            )

    file.write( conf_unknown )
    file.write( conf_wildcards )
    file.close()
    
genWildcards()