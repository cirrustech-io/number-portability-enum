#!/usr/bin/python
'''
This script will empty the current ported_numbers table and
add generate a full BIND9 zone configuration file
from ported numbers

Created on Jul 23, 2011

@author: Ali Khalil
'''

from ported.dump.PortedDump import NpcdbDump

try:
    port_dump = NpcdbDump()
    records = port_dump.getMySQL()
    if ( records == False ):
        raise
    naptr_configuration = port_dump.genNAPTR(records)
    zone_configuration = port_dump.genZoneConf(naptr_configuration)
    print zone_configuration
    port_dump.log('''Script Ended Successfully.''')
except:
    port_dump.log('''Script Ended with Exception.''')
    
