'''
Created on Jul 6, 2011

@author: Ali Khalil
'''

import sys
import syslog
import datetime
import os
import re
import commands
# TODO: import MySQLdb here or initialize in to self
# TODO: Only regenerate if sequence number changes 
# TODO: Check sequence from sql database

                                    
class NpcdbDump(object):

    def __init__(self):
        '''
        Initialize the NpcdbDump Object
        '''
        try:
            # SFTP related configuration parameters
            self.ssh_host = 'ftp.npcs.bh'   # Production Site
            #self.ssh_host = 'wwwnpbh.test.systor.st'    # Old Dummy Site (Currently migrating, so using old one which is stable)
            self.ssh_port = 22
            self.ssh_user = 'ftp_conb'      # Username
            self.ssh_pass = 'pfBy4829'      # Production password
            # self.ssh_pass = 'huNp6384'     # Dummy password
            self.ssh_timeout = 5
            self.remote_dir = '/synchronization/'
            self.local_dir = '/var/np/npcs-dump/'
            
            # ENUM related configuration parameters
            self.access_servers = ('80.88.247.115', '80.88.247.135', '80.88.247.12')
            self.servers_pref = 10
            self.domain = 'enum.2connectbahrain.com.'
            self.zone_contact = 'ali.khalil.2connectbahrain.com.'
            self.enum_ip = '80.88.247.23'
            self.enum_reject_code = 'reject31.com'
            
            # MySQL related configuration parameters
            self.mysql_host = 'np.2connectbahrain.com'
            #self.mysql_host = 'localhost'
            self.mysql_user = 'np'
            self.mysql_passwd = 'K9Q4d869hYD4G3pM'
            self.mysql_db = 'number-portability'
            self.mysql_table = 'ported_numbers'
            
            # Loaded records count
            self.count = -1
            
            # Wildcard NAPTR related configuration parameters
            # The ranges for phone numbers e.g. 33xxxxxx, 36xxxxxx, 39xxxxxx, 16xxxxxx, 17xxxxxx
            self.wildcards_ranges = [ 32, 33, 34, 36, 37, 38, 39, 66, 13, 14, 15, 16, 17 ]
            # Depth of the wildcards. NSN is 8 digits, 2 digits used in defining ranges. Remainder 6 digits to generated
            self.wildcards_depth = 6
            # Digits in the numbers (duh!)
            self.wildcards_digits = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]

            # Process ID for inclusion in logs
            self.pid = os.getpgid(0)
            
            self.runtime = datetime.datetime.now()
            
            # Currently run script file name
            self.fname = sys.argv[0]
            
            
            # BIND9 Configuration file parameters
            #self.bind9_file = '''/var/bind/db.enum.2c'''
            self.bind9_file = '''/tmp/db.enum.2c'''
            
            
            syslog.openlog('''port_dump[%s]'''%(self.pid))
            
            self.log('Initialized NpcdbDump instance.')
             
        except:
            self.log('''INIT: Error - Couldn't initialize instance.''')
    
    
    def __del__(self):
        '''
        Celanup
        '''
        self.log('''Ending process''')
        

    def getFile(self):
        '''
        Download a file from SFTP
        '''
        # Files
        filename = self.dumpFileToday()
        self.log('''FILE: Today's file name is ''' + filename)
        remote_file = self.remote_dir + filename
        local_file = self.local_dir + filename
        
        # Check for bad file and stop if found
        if ( os.path.exists(local_file) and self.zipTest(local_file)==True ):
            #self.log('''FILE: Yesterday's dump exists locally and is in good condition''')
            self.log('''FILE: Yesterday's dump exists locally and is in good condition''')
            return filename    # Bad file
        else:
            self.log('''FILE: Error - File does not exist locally.''')
            pass
    
        try:
            self.log('SFTP: Attempting SFTP file download for ' + self.ssh_host + ':' + remote_file)
            import paramiko
            #self.log('''SFTP: Set log file''')
            paramiko.util.log_to_file(self.local_dir + filename + '.log')
            # Specify the connection settings/parameters
            #self.log('''SFTP: Set Transport''')
            transport = paramiko.Transport((self.ssh_host,self.ssh_port))
            # Specify the connection credentials
            #self.log('''SFTP: Set Connection Credentials''')
            transport.connect( username=self.ssh_user, password=self.ssh_pass, timeout=self.ssh_timeout )
            # Start the connection
            #self.log('''SFTP: Initiate SFTP Client''')
            sftp = paramiko.SFTPClient.from_transport(transport)
            # Perform get
            #self.log('''SFTP: Get the file''')
            sftp.get(remote_file,local_file)
            # Clean up
            #self.log('''SFTP: Close and clean up''')
            sftp.close()
            transport.close()
            
            self.log('''SFTP: File downlaoded successfullly.''')
            return local_file

        except:
            self.log('''SFTP: Error - Could not download file from server.''')
            print "Unexpected error:", sys.exc_info()[0]
            return False
        
    
    def zipTest(self, file_location):
        from zipfile import ZipFile
        
        try:
            # Open Zip file
            zipper = ZipFile(file_location, 'r');
            # Test Zip file
            #if ( zipper.is_zipfile() and zipper.testzip()==None ):
            if zipper.testzip()==None:
                #print "zip file ok"
                self.log('''ZIP: File integrity check success.''')
                return True
            else:
                #print "zip file NOT ok"
                self.log('''ZIP: Problem with file - Unidentified''')
                return False
        except:
            #print "zipTest: Exception Thrown"
            self.log('''ZIP: Error - Exception thrown''')
            return False
        
        
    def readZipContent(self, file_location):
        '''
        Extract CSV file from Zip archive and read contents
        '''
        try:
            from zipfile import ZipFile
            # Open Zip file
            zipper = ZipFile(file_location, 'r');
            # Get file list from Zip
            files = zipper.namelist();
            # Read first file (only one file should exists)
            contents = zipper.read( files[0] )
            # Return the contents of the CSV file
            self.log('''ZIP: Contents read from file''')
            return contents
        except:
            self.log('''ZIP: Error - Failed to read file contents.''')
            return False
    
    
    def processCSV(self, contents):
        '''
        Extract useable data rows from the CSV file
        namely, skip the first row which has timestamp and record count
        Validate that record count is accurate by comparing first row information with total rows
        '''
        try:
            # Split each row into its own entity
            lines = contents.splitlines()
            # Get the record count from first row
            records_total = int(lines[0].split(';')[1])
            # Number of rows seen in list
            records_count = int(len(lines)-1)
            
            # Match record count from first row with rows seen in data ste
            if records_total == records_count:
                # Valid data: Row counts match
                
                # Initialize holder for records data
                records = []

                # Return all rows except the first row. It has timestamp and number of records only
                for line in lines[1::]:
                    # Break up CSV data and add to list
                    records.append( line.split(';') )
                
                # Set the instances records count
                self.count = records_total
                
                self.log('''CSV: Records detected - ''' + str(records_total))
                return records     
            else:
                self.log('''CSV: Error - Record counts mismatch. indicated:detected::''' + str(records_total) + ':' + str(records_count))
                return False
        except:
            self.log('Error - Processing CSV')
            return False
    
        
    def dumpFileToday(self):
        '''
        What is the filename for today?
        Filename Format:    port_dump_YYYY-MM-DD.zip
        '''
        try:
            today = datetime.date.today()-datetime.timedelta(1)
            filename = '%s%s%s'%('port_dump_',today,'.zip')
            return filename
        except:
            self.log('Filename: Error - something really wrong')
            return False
    
 
    def loadSQL (self, records):
        '''
        Load a fresh dump in to the SQL Database after emptying it.
        '''
        #print records
        try:
            insert_statement = '''INSERT INTO %s( port_id, service_type, number, subscription_network, donor_id, routing_number, port_datetime) values\n'''%(self.mysql_table)
            for record in records:
                port_id = record[0]
                service_type = record[1]
                number = record[2]
                subscription_network = record[3]
                donor_id = record[4]
                routing_number = record[5]
                port_datetime = record[6]
                insert_statement += '''( '%s', %s, %s, '%s', '%s', '%s', '%s'),\n'''%(port_id, service_type, number, subscription_network, donor_id, routing_number, port_datetime)
            
            # Remove the last comma+newline and add terminator ';' to statement
            query_insert = insert_statement.rstrip(",\n") + ';'
        
            import MySQLdb
            db = MySQLdb.connect( host=self.mysql_host, user=self.mysql_user, passwd=self.mysql_passwd, db=self.mysql_db)
            cursor = db.cursor()
            query_empty = '''DELETE FROM %s WHERE 1=1;'''%(self.mysql_table)
            cursor.execute( query_empty )
            cursor.execute( query_insert )
            db.commit()
            cursor.close()
            self.log( '''SQL: Insert committed adding %s records.'''%(str(cursor.rowcount)) ) 
        except:
            db.rollback()
            error = ''''SQL: Error - Couldn't insert in to database table'''
            self.log(error)
            #import sys
            #print "Unexpected error:", sys.exc_info()[0]
            return False
        
        db.close()
        return query_insert
        
    
    def getMySQL(self):
        '''
        Retrieve data from MySQL database
        '''
        try:
            self.log('''SQL: Retrieving records from database.''')
            data = self.sql_run('''SELECT * FROM %s'''%(self.mysql_table))
            self.count = data['count']
            return data['records']
        except:
            error = "SQL: Error - Could not read database table %s."%(self.mysql_table)
            self.log( error )
            return False
        
    
    def genNAPTR(self, records):
        '''
        Generate the BIND configuration
        '''
        self.log('''NAPTR: Starting generation of BIND9 NAPTR records.''')
        
        pattern = re.compile('(.)')
        
        configuration = ''
        
        try:
            for record in records:
                number = str(record[2])
                number_reversed = number[::-1]
                routing = record[5].upper()
                preference = self.servers_pref
                
                # Generate an NAPTR record for each access server with increasing preference
                for server in self.access_servers:
                    entry = pattern.sub('\\1.',number_reversed)
                    routed_number = routing + number + '@' + server
                    configuration += self.enumRecord( entry, preference, routed_number)
                    preference = preference + 1
                
            self.log('''NAPTR: Configuration Generated''')
        except:
            self.log("NAPTR: Error - Configuration could not be generated")
            return False
        
        return configuration
    
    
    def genZoneConf(self, conf_naptr):
        '''
        Return full BIND9 Zone configuration contents
        '''
        self.log('''ZONE: Starting Zone configuration creation.''')

        try:
            serial = self.bind9serial()
           
            configuration = '''
; Generated by python script with pid %s at %s

$TTL 86400
%s IN SOA ns.%s %s (
    %s      ; Serial no., based on date
    21600           ; Refresh after 6 hours
    3600            ; Retry after 1 hour
    604800          ; Expire after 7 days
    3600            ; Minimum TTL of 1 hour
)

%s     43200   IN NS   ns.%s
ns.%s  43200   IN A    %s

; Total Ported Numbers:    %s

%s


$INCLUDE /etc/bind/db.enum.wildcards
$INCLUDE /etc/bind/db.enum.persistent

'''%(self.pid, self.runtime, self.domain, self.domain, self.zone_contact, serial, self.domain, self.domain, self.domain, self.enum_ip, str(self.count), conf_naptr)
            
            self.log('''ZONE: Zone configuration created successfully.''')
            return configuration
        except:
            self.log('''ZONE: Error - Could not generate Zone configuration.''')
            return False


    def wildcard_range(self, base, length ):
        '''
        Recursive function to generate wildcards
        '''
        try:
            output = ''
            
            if length > 1:
                # Loop through each digit for this range
                for digit in self.wildcards_digits:
                    new_base = str(digit) + "." + base
                    entry = '*.' + new_base
                    output += self.enumRecord( entry, self.servers_pref, self.enum_reject_code)
                    content = self.wildcard_range( new_base, length-1 )
                    if content != False:
                        output += content
                return output
            else:
                return False
        except:
            self.log('''WILDCARDS: Error - base:%s, length:%s'''%(base,length))    
            return False
    
    
    def genWildcards(self):
        '''
        Generate Wildcard NAPTR records for all possible ranges of telephone number 
        allocated to Telecommunication companies in Bahrain by the TRA
        '''
        try:
            # Store the complete wildcards configuration here
            wildcards_configuration_ranges = ''
            
            # Set pattern for modifying the ranges to reverse format with . separating each digit
            pattern = re.compile('(.)')
            
            # Loop through each range
            for range in self.wildcards_ranges:
                # Reverse the range digits and separate with .
                range_reverse = pattern.sub('\\1.',str(range)[::-1])
                # Run recursive configuration generation
                wildcards_configuration_ranges += self.wildcard_range(range_reverse, self.wildcards_depth ) # , file)
            
            # Additional wildcards 
            wildcards_configuration_unknown = ''
            
            for domain in ( '', '3.'):
                wildcards_configuration_unknown += self.enumRecord( '*.'+domain, self.servers_pref, self.enum_reject_code)
                
            configuration = wildcards_configuration_unknown + wildcards_configuration_ranges
            return configuration
        except:
            self.log('''WILDCARDS: Error - Couldn't generate wildcards''')
            return False
    
    
    def enumRecord(self, entry, preference, value):
        try:
            wildcards_configuration_unknown = "%s%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (
                entry,
                self.domain,
                'NAPTR',
                '100',
                preference,
                '"U"',
                '"sip+E2U"',
                '"!^(.*)$!sip:'+value+'!"',
                '.'
                )
            return wildcards_configuration_unknown
        except:
            self.log('''ENUM_RECORD: Error - Couldn't generate ENUM record for entry:%s, preference:%s, value:%s'''%(entry, preference, value))
            return False
        
        
    def log(self, message):
        '''
        Output error to either stdout or syslog
        '''
        # Method options stdout or syslog
        methods = [ 'stdout', 'syslog' ]
        method = methods[1]
        try:
            if method == 'syslog':
                syslog.syslog( message )
            else:
                print message
            return True
        except:
            print '''Error - cannot log messages.'''
            print sys.exc_info()
            return False
       
    
    def bind9serial(self):
        '''
        Generate the serial number for BIN9 zone file
        '''
        try:
            #now = datetime.datetime.now()
            data = self.serial_get()
            now = data[0]
            increment = data[1]
            serial = '''%d%02d%02d%02d'''%(now.year, now.month, now.day, increment)
            return serial
        except:
            error = '''SERIAL: Error - Failed to generate serial number for zone configuration file.'''
            self.log( error )
            return False
    
    
    def serial_get(self):
        '''
        Get serial from MySQL database
        '''
        try:
            self.log('''SQL: Getting serial from database.''')
            data = self.sql_run('''SELECT date, increment, process_id, records, created FROM %s ORDER BY created DESC LIMIT 1'''%('bind'), True)
            return data['records']
        except:
            error = "SQL: Error - Could not read database table %s."%('bind')
            self.log( error )
            return False
    
    
    def serial_set(self):
        '''
        Get serial from MySQL database
        '''
        try:
            self.log('''SQL: Setting serial and updating database.''')
            today = datetime.date.today()
            now = datetime.datetime.today()
            
            #self.recordsCount() # Update records count - remove?
            
            currentSerial = self.serial_get()
            lastdate = currentSerial[0]

            if today != lastdate:
                # Start over on increment value for new day
                increment = 1
            else:
                # If same day then increment previous value
                increment = currentSerial[1]+1
            
            query = '''INSERT INTO %s (date, increment, process_id, records, created, modified) VALUES('%s', %s, %s, %s, '%s', '%s')'''%('bind', today, increment, self.pid, self.count, now, now)

            import MySQLdb
            db = MySQLdb.connect( host=self.mysql_host, user=self.mysql_user, passwd=self.mysql_passwd, db=self.mysql_db)
            cursor = db.cursor()
            cursor.execute(query)
            cursor.close()
            db.commit()
            return increment
        except:
            error = "SQL: Error - Could not set serial in table %s."%('bind')
            self.log( error )
            print sys.exc_info()
            return False
        
        
    def sql_run(self, statement, single=False):
        '''
        Retrieve data from MySQL database
        '''
        try:
            #self.log('''SQL: Executing statement.''')
            import MySQLdb
            db = MySQLdb.connect( host=self.mysql_host, user=self.mysql_user, passwd=self.mysql_passwd, db=self.mysql_db)
            cursor = db.cursor()
            cursor.execute(statement)
            if single == True:
                records = cursor.fetchone()
            else:
                records = cursor.fetchall()
            cursor.close()
            db.commit()
            #self.log('''SQL: Statement executed.''')
            data = {'count': cursor.rowcount, 'records': records}
            return data
        except:
            error = "SQL: Error - Statement run problem."
            self.log( error )
            return False
        

    def recordsCount(self):
        '''
        Get current records count
        '''
        try:
            #self.log('''starting count''')
            query = '''SELECT COUNT(*) FROM ported_numbers'''
            data = self.sql_run(query, True)
            count = data['records'][0]
            self.count = count
            return count
        except:
            error = '''SQL: Error - Could not get current records count'''
            self.log( error )
            return False
    
    
    def checkUpdate(self):
        '''
        Match current row count with last generated serial
        '''
        try:
            current = self.recordsCount()
            last = self.serial_get()[3]
            #print "%s, %s"%(current, lastUpdate)
            if current > last:
                #self.log('''New records found.''')
                return True
            else:
                error = '''UPDATE: No new records added since last update'''
                self.log( error )
                return False 
        except:
            error = '''UPDATE: Error - Could not check for update'''
            self.log( error )
            return False
        
    
    def writeBindConfig(self, content):
        '''
        Write BIN9 Configuration file
        '''
        try:
            file = open( self.bind9_file, 'w' )
            file.write( content )
            return True
        except:
            error = '''BIND9: Error - Could not write BIN9 configuration file %s'''%(self.bind9_file)
            self.log( error )
            return False
        
        
    def reloadBind(self):
        '''
        Restart the bind9 process
        '''
        try:
            command = '''service bind9 reload'''
            output = commands.getstatusoutput(command)
            #print output
            return output
        except:
            error = '''BIND9: Error - Could not reload BIND9'''
            self.log( error )
            return False
        
        
