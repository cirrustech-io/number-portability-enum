'''
Created on Jul 6, 2011

@author: Ali Khalil
'''
import unittest


class Test(unittest.TestCase):


    def testGet(self):
        print "Begin:\ttestGet()"
        from ported.dump.PortedDump import NpcdbDump
        myobj = NpcdbDump()
        print myobj.getFile()
        print "Done:\ttestGet()"

    def testZip(self):
        #pass
        print "Begin:\ttestZip()"
        from ported.dump.PortedDump import NpcdbDump
        myobj = NpcdbDump()
        rawData = myobj.readZipContent('/home/ali/tmp/npcs/' + myobj.dumpFileToday())
        #print rawData
        #return myobj.processCSV(rawData)
        lines = myobj.processCSV(rawData)
        print lines
        #for record in lines:
        #    print record.split(';')
        print "Done:\ttestZip()"
        
    def testBindConfig(self):
        print "Begin:\ttestBindConfig()"
        #pass
        from ported.dump.PortedDump import NpcdbDump
        myobj = NpcdbDump()
        rawData = myobj.readZipContent(myobj.local_dir + myobj.dumpFileToday())
        print rawData
        lines = myobj.processCSV(rawData)
        print myobj.genBIND(lines)
        print "Begin:\ttestBindConfig()"